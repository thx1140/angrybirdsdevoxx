print('Devoxx4kids Angry Birds')
local sky = display.newImageRect( 'graphics/backgrounds/CLASSIC_SKY.png', 1912, 954)
sky.x = display.contentCenterX
sky.y = display.contentCenterY

local bird = display.newImageRect( 'graphics/birds/ANGRY_BIRD_RED.png', 49, 47)
bird.x = 100
bird.y = display.contentCenterY - 50

local pig = display.newImageRect( 'graphics/pigs/PIG_NORMAL.png', 49, 47)
pig.x = display.contentWidth - 100
pig.y = display.contentCenterY - 50

local ground = display.newImageRect( 'graphics/backgrounds/CLASSIC_GROUND.png', 1328, 187)
ground.x = display.contentCenterX
ground.y = display.contentHeight-50


local block04 = display.newImageRect('graphics/blocks/BLOCK_RECT_NORMAL_M_STONE_V.png', 43, 85 )
block04.x = pig.x - 50
block04.y = pig.y + 150

local block05 = display.newImageRect('graphics/blocks/BLOCK_RECT_NORMAL_M_STONE_V.png', 43, 85 )
block05.x = pig.x + 50
block05.y = pig.y + 150

local block06 = display.newImageRect('graphics/blocks/BLOCK_RECT_NORMAL_M_STONE_V.png', 43, 85 )
block06.x = pig.x
block06.y = pig.y + 60
block06.rotation = 90

local physics = require( 'physics' )
physics.setDrawMode( "hybrid" )
physics.start()
physics.addBody( bird, 'dynamic', {friction=1, bounce= 0.5, density=2.5, radius=23})
physics.addBody( ground, 'static', {friction=1, bounce= 0, density=2})
physics.addBody( pig, 'dynamic', {friction=1, bounce= 0.5, density=0.5, radius=25})
physics.addBody( block04, 'dynamic', {friction=1, bounce= 0.5, density=0.5})
physics.addBody( block05, 'dynamic', {friction=1, bounce= 0.5, density=0.5})
physics.addBody( block06, 'dynamic', {friction=1, bounce= 0.5, density=0.5})

bird.isBullet = true

function bird:touch( event )
	if event.phase == "began" then
		display.getCurrentStage():setFocus( self )
		self.isFocus = true
		self.time = event.time
	elseif self.isFocus then
		if event.phase == "moved" then
			if self.arrow ~= nil then
				self.arrow:removeSelf( )
				self.arrow = nil
			end
			local d = math.sqrt( math.pow( event.x - self.x, 2) +  math.pow( event.y - self.y, 2))
			local x, y = event.x, event.y
			self.arrow = display.newLine( self.x, self.y, x, y)
			self.arrow.strokeWidth = 5
		elseif event.phase == "ended" then
			display.getCurrentStage():setFocus( nil )
			self.isFocus = nil
			if (self.arrow ~= nil) then
				self.arrow:removeSelf( )
				self.arrow = nil
			end
			local xForce = (1 * (event.x - bird.x))
			local yForce = (1 * (event.y - bird.y))
			self:applyLinearImpulse( xForce,yForce, bird.x, bird.y )
		end
	end
	return true
end
bird:addEventListener( "touch", bird )
